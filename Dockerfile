FROM ubuntu:20.04

ARG ARCH=x86_64
ENV DEBIAN_FRONTEND noninteractive

# Use bash as the default
SHELL ["/bin/bash", "-c"]

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install --no-install-recommends -y \
        autoconf automake libtool pkg-config clang bison cmake mercurial ninja-build samba flex texinfo time libglib2.0-dev libpixman-1-dev libarchive-dev libarchive-tools libbz2-dev libattr1-dev libcap-ng-dev \
        build-essential openssh-client git-core \
        cmake ccache ninja-build cmake-curses-gui \
        libxml2-utils ncurses-dev \
        curl git doxygen device-tree-compiler \
        u-boot-tools \
        python3-dev python3-pip python-is-python3 \
        protobuf-compiler python3-protobuf \
        libssl-dev libclang-dev libcunit1-dev libsqlite3-dev \
        gcc-aarch64-linux-gnu g++-aarch64-linux-gnu \
        cpio \
        && \
    apt-get autoremove -y && apt-get clean && \
    rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/* && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3 1

RUN adduser icecap --home /home/icecap --disabled-password --gecos ""

USER icecap

RUN pip3 install setuptools
RUN pip3 install sel4-deps
RUN pip3 uninstall libarchive
RUN pip3 install libarchive-c
RUN mkdir -p ~/.bin
RUN curl https://storage.googleapis.com/git-repo-downloads/repo > ~/.bin/repo
RUN chmod a+rx ~/.bin/repo
ENV PATH=/home/icecap/.bin:/home/icecap/cheri/output/morello-sdk/bin:/home/icecap/cheri/build/qemu-build:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

WORKDIR /home/icecap
RUN git clone https://github.com/CTSRD-CHERI/cheribuild.git
